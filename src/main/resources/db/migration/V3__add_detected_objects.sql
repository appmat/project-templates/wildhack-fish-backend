alter table image
    add column processed boolean default false ;

create table fish
(
    id           bigserial primary key,
    image_id     bigint references image (id),
    probability  double precision,
    bounding_box text
)
