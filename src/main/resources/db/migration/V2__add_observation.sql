create table observation
(
    id         bigserial primary key,
    barrier_id bigint references barrier (id),
    date       date,
    user_id    uuid
);

create table image
(
    id             bigserial primary key,
    observation_id bigint references observation (id),
    filename       text,
    data           bytea
);
