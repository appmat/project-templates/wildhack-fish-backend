package org.appmat.lapki.greeting

import io.ktor.application.*
import io.ktor.locations.*
import io.ktor.response.*
import io.ktor.routing.*
import org.appmat.lapki.plugins.routing.KtorController

class GreetingController(
    private val greetingService: GreetingService
) : KtorController {

    override val routing: Routing.() -> Unit = {
        get<Greeting> {
            call.respondText(greetingService.greet())
        }
    }
}
