package org.appmat.lapki.greeting

import org.appmat.lapki.plugins.routing.KtorController
import org.koin.dsl.module

val greetingModule = module {
    single<GreetingService> { GreetingServiceImpl() }
    single<KtorController> { GreetingController(get()) }
}
