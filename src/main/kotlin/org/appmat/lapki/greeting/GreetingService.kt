package org.appmat.lapki.greeting

interface GreetingService {
    fun greet(): String
}

