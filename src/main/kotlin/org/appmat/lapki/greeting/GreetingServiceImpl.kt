package org.appmat.lapki.greeting

class GreetingServiceImpl : GreetingService {
    override fun greet() = "Hello World!"
}
