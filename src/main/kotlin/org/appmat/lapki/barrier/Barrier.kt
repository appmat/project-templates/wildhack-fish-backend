package org.appmat.lapki.barrier

import arrow.core.*
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.request.path
import io.ktor.request.receive
import io.ktor.response.respond
import io.ktor.routing.*
import io.ktor.util.getValue
import kotlinx.serialization.Serializable
import org.appmat.lapki.plugins.routing.KtorController
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.selectBatched
import org.jetbrains.exposed.sql.transactions.transaction
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single
import org.postgresql.jdbc.PgResultSet.toLong

@Serializable
data class BarrierCreateRequest(val name: String)

@Serializable
data class BarrierUpdateRequest(val name: String)

@Serializable
data class BarrierResponse(val id: Long, val name: String) {
    constructor(barrier: BarrierDTO) : this(barrier.id, barrier.name)
}

data class BarrierDTO(val id: Long, val name: String)


object Barriers : LongIdTable("barrier") {
    val name = text("name")
}

class Barrier(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Barrier>(Barriers)

    var name by Barriers.name

    fun toDTO() = BarrierDTO(id.value, name)
    fun fromDTO(dto: BarrierDTO): Barrier {
        name = dto.name
        return this
    }
}


class BarrierController(service: BarrierService) : KtorController {

    private suspend fun ApplicationCall.respondError(error: BarrierError) {
        when (error) {
            BarrierError.NoIdProvided -> this.respond(HttpStatusCode.BadRequest, "no barrier id provided")
            is BarrierError.NotFound -> this.respond(HttpStatusCode.NotFound, "barrier ${error.id} not found")
        }
    }

    override val routing: Routing.() -> Unit = {
        get("/api/barriers") {

            val limit = call.request.queryParameters["limit"]?.toIntOrNull() ?: 10
            val offset = call.request.queryParameters["offset"]?.toLongOrNull() ?: 0L
            val list = service.list(limit, offset).map { BarrierResponse(it) }.toList()
            call.respond(list)
        }

        get("/api/barriers/{id}") {
            val id: Long by call.parameters
            service.get(id).map { BarrierResponse(it) }.fold({ call.respondError(it) }, { call.respond(it) })
        }


        post("/api/barriers") {
            val createRequest = call.receive<BarrierCreateRequest>()
            val barrierDTO = service.create(createRequest.name)
            call.respond(HttpStatusCode.Created, BarrierResponse(barrierDTO))
        }

        put("/api/barriers/{id}") {
            val id: Long by call.parameters
            service.update(BarrierDTO(id, call.receive<BarrierUpdateRequest>().name)).map { BarrierResponse(it) }
                .fold({ call.respondError(it) }, { call.respond(it) })
        }
        delete("/api/barriers/{id}") {
            val id: Long by call.parameters
            service.delete(id)
                .map { BarrierResponse(it) }
                .fold(
                    { call.respond(HttpStatusCode.NoContent, Unit) },
                    { call.respond(it) }
                )
        }
    }
}

sealed class BarrierError {
    class NotFound(val id: Long) : BarrierError();
    object NoIdProvided : BarrierError()
}

class BarrierService {
    fun create(name: String): BarrierDTO {
        return transaction {
            Barrier.new { this.name = name }.toDTO()
        }
    }

    fun update(dto: BarrierDTO): Either<BarrierError, BarrierDTO> {
        return transaction {
            Either.fromNullable(Barrier.findById(dto.id))
                .bimap({ BarrierError.NotFound(dto.id) }, { it.fromDTO(dto).toDTO() })
        }
    }

    fun get(id: Long): Either<BarrierError, BarrierDTO> {
        return transaction {
            Either.fromNullable(Barrier.findById(id)).bimap({ BarrierError.NotFound(id) }, { it.toDTO() })
        }
    }

    fun delete(id: Long): Option<BarrierDTO> {
        return transaction {
            Barrier.findById(id).toOption().tap { it.delete() }.map { it.toDTO() }
        }
    }

    fun list(limit: Int, offset: Long): List<BarrierDTO> {
        return transaction {
            Barrier.all().limit(limit, offset).notForUpdate().map { it.toDTO() }.toList()
        }
    }
}

val barrierModule = module {
    single<BarrierService>()
    single<BarrierController>() bind KtorController::class
}
