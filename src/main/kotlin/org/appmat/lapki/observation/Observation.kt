package org.appmat.lapki.observation

import ai.djl.modality.cv.ImageFactory
import ai.djl.modality.cv.output.DetectedObjects
import ai.djl.modality.cv.transform.Resize
import ai.djl.modality.cv.transform.ToTensor
import ai.djl.modality.cv.translator.YoloV5Translator
import ai.djl.repository.zoo.Criteria
import ai.djl.repository.zoo.ModelZoo
import ai.djl.training.util.ProgressBar
import arrow.core.*
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.http.HttpStatusCode
import io.ktor.http.content.PartData
import io.ktor.http.content.forEachPart
import io.ktor.http.content.streamProvider
import io.ktor.request.receiveMultipart
import io.ktor.response.respond
import io.ktor.routing.Routing
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.util.getValue
import org.appmat.lapki.barrier.Barrier
import org.appmat.lapki.barrier.Barriers
import org.appmat.lapki.plugins.routing.KtorController
import org.jetbrains.exposed.dao.LongEntity
import org.jetbrains.exposed.dao.LongEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.LongIdTable
import org.jetbrains.exposed.sql.javatime.date
import org.koin.dsl.bind
import org.koin.dsl.module
import org.koin.dsl.single
import kotlinx.datetime.LocalDate
import kotlinx.datetime.toJavaLocalDate
import kotlinx.datetime.toKotlinLocalDate
import kotlinx.datetime.toLocalDate
import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import org.jetbrains.exposed.sql.SqlExpressionBuilder.greater
import org.jetbrains.exposed.sql.SqlExpressionBuilder.inList
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.not
import org.jetbrains.exposed.sql.select
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.io.path.Path

object Observations : LongIdTable("observation") {
    val barrierId = reference("barrier_id", Barriers.id)
    val date = date("date")
    val userId = uuid("user_id")
}

object Images : LongIdTable("image") {
    val observationId = reference("observation_id", Observations.id)
    val filename = text("filename")
    val data = binary("data")
    val processed = bool("processed")
}

class Observation(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Observation>(Observations)

    var barrier by Barrier referencedOn Observations.barrierId
    var date by Observations.date
    var userId by Observations.userId
    val imageNames by ImageName referrersOn Images.observationId

    fun toDTO() = ObservationDTO(id.value, barrier.id.value, date.toKotlinLocalDate(), userId)
    fun fromDTO(dto: ObservationDTO): Observation {
        date = dto.date.toJavaLocalDate()
        userId = dto.userId
        return this
    }
}

class ImageName(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<ImageName>(Images)

    var observation by Observation referencedOn Images.observationId
    var filename by Images.filename
    var processed by Images.processed

    fun toDTO() = ImageNameDTO(id.value, filename, processed)
}

class Image(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Image>(Images)

    var observation by Observation referencedOn Images.observationId
    var filename by Images.filename
    var data by Images.data
    var processed by Images.processed

    fun toDTO() = ImageDTO(id.value, filename, data, processed)
    fun fromDTO(dto: ImageDTO): Image {
        filename = dto.filename
        data = dto.data
        processed = dto.processed
        return this
    }
}


data class ObservationDTO(
    val id: Long, val barrierId: Long, val date: LocalDate, val userId: UUID
)

data class ObservationWithFishDTO(
    val id: Long, val barrierId: Long, val date: LocalDate, val userId: UUID, val fish: Long, val processed: Boolean
)

@Serializable
data class ObservationResponse(
    val id: Long, val barrierId: Long, val date: LocalDate, val userId: String
) {
    constructor(dto: ObservationDTO) : this(dto.id, dto.barrierId, dto.date, dto.userId.toString())
}

@Serializable
data class ObservationWithFishResponse(
    val id: Long, val barrierId: Long, val date: LocalDate, val userId: String, val fish: Long, val processed: Boolean
) {
    constructor(dto: ObservationWithFishDTO) : this(
        dto.id,
        dto.barrierId,
        dto.date,
        dto.userId.toString(),
        dto.fish,
        dto.processed
    )
}

@Serializable
data class ImageResponse(
    val id: Long, val filename: String, val processed: Boolean
) {
    constructor(dto: ImageNameDTO) : this(dto.id, dto.filename, dto.processed)
}

@Serializable
data class ObservationDetailedResponse(
    val observation: ObservationResponse, val images: List<ImageResponse>
)

data class ImageNameDTO(
    val id: Long, val filename: String, val processed: Boolean
)

data class ImageDTO(
    val id: Long, val filename: String, val data: ByteArray, val processed: Boolean
) {
    constructor(filename: String, data: ByteArray) : this(0L, filename, data, false)

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ImageDTO

        if (filename != other.filename) return false
        if (!data.contentEquals(other.data)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = filename.hashCode()
        result = 31 * result + data.contentHashCode()
        return result
    }
}


class ObservationController(observationService: ObservationService, imageService: ImageService) : KtorController {

    private suspend fun ApplicationCall.respondError(error: ObservationError) {
        when (error) {
            is ObservationError.BarrierNotFound -> this.respond(
                HttpStatusCode.NotFound, "barrier ${error.id} not found"
            )
            is ObservationError.ObservationNotFound -> this.respond(
                HttpStatusCode.NotFound, "observation ${error.id} not found"
            )
        }
    }

    override val routing: Routing.() -> Unit = {
        get("/api/barriers/{barrierId}/observations") {
            val limit = call.request.queryParameters["limit"]?.toIntOrNull() ?: 10
            val offset = call.request.queryParameters["offset"]?.toLongOrNull() ?: 0L
            val list = observationService.list(limit, offset).map { ObservationWithFishResponse(it) }
            call.respond(list)
        }

        post("/api/barriers/{barrierId}/observations") {
            val multipart = call.receiveMultipart()
            val filesToStore = mutableListOf<PartData.FileItem>()
            val dataMap = mutableMapOf<String, String>()
            multipart.forEachPart {
                when (it) {
                    is PartData.FormItem -> dataMap[it.name!!] = it.value
                    is PartData.FileItem -> filesToStore.add(it)
                }
            }
            val barrierId: Long by call.parameters
            transaction {
                observationService.create(
                    ObservationDTO(
                        0L, barrierId, dataMap["date"]!!.toLocalDate(), UUID.randomUUID()
                    )
                ).tap { observation ->
                    filesToStore.forEach {
                        imageService.create(
                            ImageDTO(
                                it.originalFileName!!, it.streamProvider().readBytes()
                            ), observation.id
                        )
                    }
                }
            }.map { ObservationResponse(it) }
                .fold({ call.respondError(it) }, { call.respond(HttpStatusCode.Created, it) })
        }

        get("/api/barriers/{barrierId}/observations/{id}") {
            val id: Long by call.parameters
            observationService.get(id).map {
                ObservationDetailedResponse(
                    ObservationResponse(it.first), it.second.map(::ImageResponse)
                )
            }.fold({ call.respondError(it) }, { call.respond(it) })
        }
    }
}

sealed class ObservationError {
    class BarrierNotFound(val id: Long) : ObservationError()
    class ObservationNotFound(val id: Long) : ObservationError()
}

class ImageService {
    fun create(dto: ImageDTO, observationId: Long): Either<ObservationError, ImageDTO> {
        return transaction {
            Either.fromNullable(Observation.findById(observationId))
                .mapLeft { ObservationError.ObservationNotFound(observationId) }.map {
                    Image.new {
                        observation = it
                        fromDTO(dto)
                    }
                }.map { it.toDTO() }
        }
    }

    fun findNextImage(): Option<ImageDTO> {
        return transaction {
            Image.find { not(Images.processed) }.firstOrNull().toOption().map { it.toDTO() }
        }
    }

    fun markProcessed(id: Long) {
        transaction {
            Image.findById(id)?.processed = true
        }
    }

}

class ObservationService {

    fun get(id: Long): Either<ObservationError, Pair<ObservationDTO, List<ImageNameDTO>>> = transaction {
        Either.fromNullable(Observation.findById(id)).mapLeft { ObservationError.ObservationNotFound(id) }
            .map { it.toDTO() to it.imageNames.map(ImageName::toDTO) }
    }

    fun create(dto: ObservationDTO): Either<ObservationError, ObservationDTO> {
        return transaction {
            Either.fromNullable(Barrier.findById(dto.barrierId)).mapLeft { ObservationError.BarrierNotFound(dto.id) }
                .map { foundBarrier ->
                    Observation.new {
                        barrier = foundBarrier
                        fromDTO(dto)
                    }
                }.map { it.toDTO() }
        }
    }

    fun list(limit: Int, offset: Long) = transaction {
        Observation.all().limit(limit, offset).notForUpdate().map {
            val list = it.imageNames.map { it.id.value }
            val count = Fishes.innerJoin(Images)
                .select((Images.id inList list) and (Fishes.probability greater 0.5))
                .count()
            ObservationWithFishDTO(
                it.id.value,
                it.barrier.id.value,
                it.date.toKotlinLocalDate(),
                it.userId,
                count,
                it.imageNames.all { it.processed })
        }
    }

}


val observationModule = module {
    single<FishService>()
    single<ObservationService>()
    single<ImageService>()
    single<ObservationController>() bind KtorController::class
    single<PredictionProcessWrapper>()
}

class PredictionProcessWrapper(
    val imageService: ImageService, val fishService: FishService
) {

    private val model by lazy {
        val translator = YoloV5Translator.builder().addTransform(Resize(1500)).addTransform(ToTensor())
            .optSynset(listOf("n128 fish")).build()

        File("fish.jit").writeBytes(javaClass.classLoader.getResourceAsStream("fish.jit").readBytes())
        val criteria = Criteria.builder().optApplication(ai.djl.Application.CV.OBJECT_DETECTION).setTypes(
            ai.djl.modality.cv.Image::class.java, DetectedObjects::class.java
        ) // defines input and output data type
            .optModelPath(Path("./")) // search models in specified path
            .optModelName("fish.jit") // specify model file prefix
            .optProgress(ProgressBar()).optTranslator(translator).build()
        ModelZoo.loadModel(criteria)
    }

    private val imageFactory = ImageFactory.getInstance()

    private val executor = Executors.newScheduledThreadPool(1);

    fun start() {
        executor.scheduleWithFixedDelay({
            fun detect(image: ai.djl.modality.cv.Image) = model.newPredictor().use { predictor ->
                val predict = predictor.predict(image)
                println("success")
                predict
            }

            val preprocess = { array: ByteArray ->
                ({ array.inputStream() } andThen (imageFactory::fromInputStream)).invoke()
            }


            imageService.findNextImage().map {
                val image = preprocess(it.data)
                val detectedObjects = detect(image)
                val fishDtos = detectedObjects.toFishDtos(it.id)
                it.id to fishDtos
            }.map {
                transaction {
                    fishService.create(it.second)
                    imageService.markProcessed(it.first)
                }
            }
        }, 1L, 1000L, TimeUnit.MILLISECONDS)
    }
}


fun DetectedObjects.toFishDtos(imageId: Long): List<FishDto> =
    items<DetectedObjects.DetectedObject>().map { FishDto(imageId, it.probability, it.boundingBox.toDto()) }

fun ai.djl.modality.cv.output.Point.toDto() = Point(this.x, this.y)

fun ai.djl.modality.cv.output.BoundingBox.toDto() =
    BoundingBox(this.path.map { it.toDto() }, this.bounds.width, this.bounds.height)


@Serializable
data class Point(
    val x: Double, val y: Double
)

@Serializable
data class BoundingBox(
    val corners: List<Point>, val width: Double, val height: Double
)

data class FishDto(
    val imageId: Long, val probability: Double, val boundingBox: BoundingBox
)


object Fishes : LongIdTable("fish") {
    val image = reference("image_id", Images)
    val probability = double("probability")
    val boundingBox = text("bounding_box")
}


class Fish(id: EntityID<Long>) : LongEntity(id) {
    companion object : LongEntityClass<Fish>(Fishes)

    var image by Image referencedOn Fishes.image
    var probability by Fishes.probability
    var boundingBox by Fishes.boundingBox
}


class FishService {

    fun create(dtos: List<FishDto>) {
        transaction {
            val imageName = Image.findById(dtos.first().imageId) ?: error("Could not find image")
            dtos.forEach { fish ->
                Fish.new {
                    image = imageName
                    probability = fish.probability
                    boundingBox = Json.encodeToString(fish.boundingBox)
                }
            }
        }
    }
}
