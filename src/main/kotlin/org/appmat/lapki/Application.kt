package org.appmat.lapki

import io.ktor.application.*
import io.ktor.server.netty.*
import org.appmat.lapki.barrier.barrierModule
import org.appmat.lapki.observation.PredictionProcessWrapper
import org.appmat.lapki.observation.observationModule
import org.appmat.lapki.plugins.di.configureDI
import org.appmat.lapki.plugins.healthcheck.configureHealthChecks
import org.appmat.lapki.plugins.monitoring.configureMonitoring
import org.appmat.lapki.plugins.routing.configureRouting
import org.appmat.lapki.plugins.serialization.configureSerialization
import org.koin.core.module.Module
import org.koin.ktor.ext.getKoin
import ru.appmat.plugins.cors.configureCORS
import ru.appmat.plugins.database.configureDatabase

fun main(args: Array<String>): Unit = EngineMain.main(args)

/**
 * Please note that you can use any other name instead of *module*.
 * Also note that you can have more then one modules in your application.
 * */
@Suppress("unused") // Referenced in application.conf
@JvmOverloads
fun Application.module(testing: Boolean = false, koinModules: List<Module> = listOf(barrierModule, observationModule)) {
    configureDI(koinModules)
    configureRouting()
    configureMonitoring()
    configureSerialization()
    configureHealthChecks()
    configureDatabase(testing)
    configureCORS()

    val predictionProcessWrapper = getKoin().get<PredictionProcessWrapper>()
    predictionProcessWrapper.start()
}
