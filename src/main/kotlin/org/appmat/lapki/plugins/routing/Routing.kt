package org.appmat.lapki.plugins.routing

import io.ktor.application.*
import io.ktor.locations.*
import io.ktor.routing.*
import org.koin.ktor.ext.getKoin

fun Application.configureRouting() {
    install(Locations)

    routing {
        getKoin().getAll<KtorController>()
            /**
             * prevent from registering one controller twice
             */
            .distinct()
            .forEach { it.routing(this) }
    }
}
