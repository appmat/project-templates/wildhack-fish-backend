package org.appmat.lapki.plugins.routing

import io.ktor.routing.*

interface KtorController {
    val routing: Routing.() -> Unit
}
