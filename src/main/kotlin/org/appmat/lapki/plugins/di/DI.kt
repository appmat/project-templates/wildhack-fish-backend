package org.appmat.lapki.plugins.di

import io.ktor.application.*
import org.koin.core.logger.Level
import org.koin.core.module.Module
import org.koin.ktor.ext.Koin
import org.koin.logger.slf4jLogger

fun Application.configureDI(koinModules: List<Module>) {
    install(Koin) {
        slf4jLogger(Level.NONE)
        modules(koinModules)
    }
}
