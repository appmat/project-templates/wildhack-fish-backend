package org.appmat.lapki.plugins.healthcheck

import io.ktor.application.*
import ktor_health_check.Health

/**
 * installs liveness and readiness probe endpoints for kubernetes
 */
fun Application.configureHealthChecks() {
    install(Health) {
        // add custom readiness checks if needed
        /*
        readyCheck("database") {
            db.isAlive
        }*/
    }
}
