package org.appmat.lapki.plugins.monitoring

import io.ktor.application.*
import io.ktor.config.*
import io.ktor.features.*
import io.ktor.locations.*
import io.ktor.metrics.micrometer.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import io.micrometer.prometheus.PrometheusConfig
import io.micrometer.prometheus.PrometheusMeterRegistry
import org.slf4j.event.Level

fun Application.configureMonitoring() {
    val config = environment.config.config("ktor.monitoring")

    configureCallLogging(config)
    configureMetrics(config)
}

fun Application.configureCallLogging(config: ApplicationConfig) {
    install(CallLogging) {
        val callLoggingConfig = config.config("callLogging")

        level = Level.INFO
        callLoggingConfig.propertyOrNull("level")?.let {
            level = Level.valueOf(it.getString())
        }

        callLoggingConfig.propertyOrNull("methodsToIgnoreRegex")?.let {
            val methodsToIgnoreRegex = it.toString().toRegex()

            filter { call ->
                methodsToIgnoreRegex.matches(call.request.path()).not()
            }
        }
    }
}

fun Application.configureMetrics(config: ApplicationConfig) {
    val appMicrometerRegistry = PrometheusMeterRegistry(PrometheusConfig.DEFAULT)
    install(MicrometerMetrics) {
        registry = appMicrometerRegistry
    }

    routing {
        get<Metrics> {
            call.respond(appMicrometerRegistry.scrape())
        }
    }
}
