package org.appmat.lapki.plugins.monitoring

import io.ktor.locations.*

@Location("/metrics")
class Metrics
