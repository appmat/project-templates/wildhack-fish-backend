package org.appmat.lapki.greeting

import io.kotest.core.spec.style.StringSpec
import io.kotest.koin.KoinListener
import io.kotest.matchers.shouldBe
import org.koin.test.KoinTest
import org.koin.test.inject

//class GreetingKoinTest : KoinTest, StringSpec() {
//    override fun listeners() = listOf(KoinListener(greetingModule))
//    val helloService by inject<GreetingService>()
//
//    init {
//        "hello service returns greeting" {
//            helloService.greet() shouldBe "Hello World!"
//        }
//    }
//}
