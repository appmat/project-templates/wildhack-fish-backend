import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val koin_version: String by project
val ktor_version: String by project
val kotest_version: String by project
val kotlin_version: String by project
val logback_version: String by project
val prometeus_version: String by project
val exposed_version: String by project
val flyway_version: String by project
val postgresql_version: String by project
val hikari_version: String by project

plugins {
    application
    kotlin("jvm") version "1.6.0"
    id("org.jetbrains.kotlin.plugin.serialization") version "1.6.0"
}

group = "org.appmat"
version = "0.0.1"
application {
    mainClass.set("io.ktor.server.netty.EngineMain")
}

repositories {
    mavenCentral()
    maven(url = "https://jitpack.io")
}

dependencies {
    // Ktor
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-serialization:$ktor_version")
    implementation("io.ktor:ktor-metrics-micrometer:$ktor_version")
    implementation("io.micrometer:micrometer-registry-prometheus:$prometeus_version")
    implementation("io.ktor:ktor-metrics:$ktor_version")
    implementation("io.ktor:ktor-locations:$ktor_version")
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("ch.qos.logback:logback-classic:$logback_version")
    implementation("io.sentry:sentry-logback:5.4.3")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")

    // Health check
    implementation("com.github.zensum:ktor-health-check:011a5a8")


    implementation("org.flywaydb:flyway-core:$flyway_version")
    implementation("org.jetbrains.exposed:exposed-core:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-dao:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-jdbc:$exposed_version")
    implementation("org.jetbrains.exposed:exposed-java-time:$exposed_version")
    implementation("org.jetbrains.kotlinx:kotlinx-datetime:0.3.1")
    implementation("org.postgresql:postgresql:$postgresql_version")
    implementation("com.zaxxer:HikariCP:$hikari_version")
    implementation("io.insert-koin:koin-core:$koin_version")
    implementation("io.insert-koin:koin-ktor:$koin_version")
    implementation("io.insert-koin:koin-logger-slf4j:$koin_version")

    implementation(platform("io.arrow-kt:arrow-stack:1.0.1"))
    implementation("io.arrow-kt:arrow-core")

    // Koin
    implementation("io.insert-koin:koin-ktor:$koin_version")
    implementation("io.insert-koin:koin-logger-slf4j:$koin_version")
    testImplementation("io.insert-koin:koin-test-junit5:$koin_version")

    implementation(platform("ai.djl:bom:0.14.0"))
    implementation("ai.djl.pytorch:pytorch-engine")
    implementation("ai.djl.pytorch:pytorch-model-zoo")
    implementation("ai.djl.pytorch:pytorch-native-auto:1.9.1")
//    implementation("ai.djl.pytorch:pytorch-native-cpu:1.9.1:linux-x86_64")
    implementation("ai.djl.opencv:opencv:0.14.0")

    // Kotest
    testImplementation("io.kotest:kotest-runner-junit5:$kotest_version")
    testImplementation("io.kotest:kotest-assertions-core:$kotest_version")
    testImplementation("io.kotest:kotest-property:$kotest_version")
    testImplementation("io.kotest:kotest-property-datetime:$kotest_version")
    testImplementation("io.kotest.extensions:kotest-extensions-koin:1.0.0")
    testImplementation("io.kotest.extensions:kotest-assertions-ktor:1.0.3")
}

/**
 * stage task is executed in buildpack jvm pipeline
 * @see <a href="https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku">https://devcenter.heroku.com/articles/deploying-gradle-apps-on-heroku</a>
 */
tasks.register("stage") {
    dependsOn("installDist")
    /**
     * generates the Procfile that specifies a command to run the application
     */
    doLast {
        val relativeInstallationPath = tasks.installDist.get().destinationDir.relativeTo(project.projectDir)
        File("Procfile").writeText(
            """
                web: ./$relativeInstallationPath/bin/${project.name}
            """.trimIndent()
        )
    }
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions.freeCompilerArgs += "-Xopt-in=io.ktor.locations.KtorExperimentalLocationsAPI"
}
